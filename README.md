# Simple Web Docker
=============

Application to validate deploy of web application with docker on jenkins.

1 - Build docker application
```ruby
mvn package docker:build
```


//TODO NOW
------------
2 - Add docker to mongodb

3 - Separate profiles to pipeline

4 - Create integration tests to persist and validade data on mongo docker


//Future
------------
Pipeline on Jenkins

References:
------------

https://github.com/picadoh/boots
